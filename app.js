document.addEventListener("DOMContentLoaded", function () {
    // Function to update the highlighted plan based on the user count
    function updateHighlightedPlan(userCount) {
        // Remove the 'highlighted' class from all plans
        var pricingTables = document.querySelectorAll(".pricing-table");
        pricingTables.forEach(function (table) {
            table.classList.remove("highlighted");
        });

        // Determine which plan to highlight based on user count
        var freePlan = document.getElementById("freePlan");
        var proPlan = document.getElementById("proPlan");
        var enterprisePlan = document.getElementById("enterprisePlan");

        if (userCount >= 0 && userCount < 10) {
            freePlan.classList.add("highlighted");
        } else if (userCount >= 10 && userCount < 20) {
            proPlan.classList.add("highlighted");
        } else if (userCount >= 20) {
            enterprisePlan.classList.add("highlighted");
        }
    }

    // Event listener for the user slider
    var userSlider = document.getElementById("userSlider");
    userSlider.addEventListener("change", function () {
        var userCount = parseInt(userSlider.value);
        updateHighlightedPlan(userCount);
    });
});
